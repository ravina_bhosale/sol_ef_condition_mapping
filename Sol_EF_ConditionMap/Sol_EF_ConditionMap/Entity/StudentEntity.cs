﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_ConditionMap.Entity
{
    public class StudentEntity
    {
        public int StudentId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public DateTime? DOB { get; set; }

        public int? Age { get; set; }

        public String Address { get; set; }
    }
}
