﻿using Sol_EF_ConditionMap.EF;
using Sol_EF_ConditionMap.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_ConditionMap
{
   public class StudentDal
    {
        #region Declaration
        private StudentEntities dbObj = null;
        #endregion

        #region Constructor
        public StudentDal()
        {
            dbObj = new StudentEntities();
        }
        #endregion

        #region public method

        public async Task<IEnumerable<StudentEntity>> GetStudentdata()
        {
            try
            {
                return await Task.Run(() =>
                {
                    var getQuery =
                    dbObj
                    .tblStudents
                    .AsEnumerable()
                    .Select((letblstudentObj) => new StudentEntity()
                    {
                        StudentId = (int)letblstudentObj.StudentId,
                        FirstName = letblstudentObj.FirstName,
                        LastName = "Shinde",
                        DOB = letblstudentObj.DOB,
                        Age = letblstudentObj.Age,
                        Address = letblstudentObj.Address
                    }
                    ).ToList();
                    return getQuery;
                });
            }
            catch(Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
