﻿using Sol_EF_ConditionMap.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_EF_ConditionMap
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => {

                IEnumerable<StudentEntity> listStudentData =
                    await new StudentDal().GetStudentdata();

            }).Wait();
        }
    }
}
